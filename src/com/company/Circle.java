package com.company;

/**
 * Клас Circle з полями: радіус, довжина окружності, площа та колір
 */
public class Circle {
    private double radius;
    private double length;
    private double area;
    private String colour;

    /**
     * @param radius
     */
    public Circle(double radius) {
        this.radius = radius;
    }

    /**
     * конструктор Circle з параметрами колір та радіус
     *
     * @param colour
     * @param radius
     */

    public Circle(String colour, double radius) {
        this.colour = colour;
        this.radius = radius;

        area = Math.PI * (radius * radius);
        length = Math.PI * (2 * radius);

    }

    /**
     * гетер для радіуса
     *
     * @return
     */
    public double getRadius() {

        return radius;
    }

    /**
     * сетер для радіуса
     *
     * @param radius
     */
    public void setRadius(double radius) {

        this.radius = radius;
    }

    /**
     * @return
     */
    public double getLength() {

        return length;
    }

    /**
     * cетер для довжини окружності
     *
     * @param length
     */
    public void setLength(double length) {

        this.length = length;
    }

    /**
     * гетер для площі кола
     *
     * @return
     */
    public double getArea() {

        return area;
    }

    /**
     * сетер для площі кола
     *
     * @param area
     */
    public void setArea(double area) {

        this.area = area;
    }

    /**
     * гетер для кольору
     *
     * @return
     */
    public String getColour() {

        return colour;
    }

    /**
     * сетер для кольору
     *
     * @param colour
     */
    public void setColour(String colour) {

        this.colour = colour;
    }
}
