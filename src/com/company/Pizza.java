package com.company;

public class Pizza {

    /**
     * Цей клас створює атрибути піци: основу типу Circle, склад, назву та ціну
     */
    Circle basic;
    String composition;
    String name;
    double cost;

    /**
     * конструктор за параметрами та об'єкт класу Circle з діаметром піци
     *
     * @param composition
     * @param name
     * @param cost
     */
    public Pizza(String composition, String name, double cost, double diameter) {
        this.composition = composition;
        this.name = name;
        this.cost = cost;
        basic = new Circle(diameter / 2);
    }
}
